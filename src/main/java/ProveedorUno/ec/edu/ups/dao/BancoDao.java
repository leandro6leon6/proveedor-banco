package ProveedorUno.ec.edu.ups.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ProveedorUno.ec.edu.ups.model.EntidadBancaria;

@Stateless
public class BancoDao {

	@PersistenceContext
	private EntityManager em;
	
	public EntidadBancaria read(int cuenta) {
		return em.find(EntidadBancaria.class, cuenta);
	}
	
}
