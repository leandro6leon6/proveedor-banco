package ProveedorUno.ec.edu.ups.ws;

import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;

import ProveedorUno.ec.edu.ups.on.BancoOn;

@WebService
public class Servicio {

	@Inject
	private BancoOn banco;
	
	@WebMethod
	public double Peticion(int cuenta, double valor) {
		try {
			return banco.Saldo(cuenta, valor);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0.0;
	}
}
