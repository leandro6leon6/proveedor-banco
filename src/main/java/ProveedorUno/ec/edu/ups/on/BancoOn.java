package ProveedorUno.ec.edu.ups.on;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ProveedorUno.ec.edu.ups.dao.BancoDao;
import ProveedorUno.ec.edu.ups.model.EntidadBancaria;

@Stateless
public class BancoOn {
	
	@Inject
	private BancoDao banco;
	
	public double Saldo(int cuenta, double valor) throws Exception {
		EntidadBancaria cuentaRecueprada = banco.read(cuenta);
		
		if(cuentaRecueprada!=null) {
			double saldo = cuentaRecueprada.getSaldo();
			if(saldo>=valor) {
				return valor;
			}
		}
		return 0.0;
	}

}
